
<?php
// Crear conexión
$host = "localhost";
$user = "root";
$password = "";
$database = "empresa_a";

try {
    $conn = mysqli_connect($host, $user, $password, $database);
    
} catch (Exception $e) {
    echo "Error con el método mysqli_connect ".$e;
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <table>
        <tr>
            <td>id</td>
            <td>nombre</td>
            <td>salario</td>
            <td>departamento</td>
        </tr>
        <?php
        $sql_query = "SELECT e.id as id, e.nombre as nombre, e.salario as salario, d.nombre as departamento
                        FROM empleado e
                        JOIN departamento d
                        ON (e.id_departamento = d.id)";
        $resultado = mysqli_query($conn, $sql_query);

        while($mostrar=mysqli_fetch_array($resultado)){
        ?>
        <tr>
            <td>
                <form action="crud.php" method="post">
                   <td><input type="text" name="id" size=4px value=<?php echo $mostrar['id']?> readonly></td>
                    <td><?php echo $mostrar['nombre'] ?></td>
                    <td><?php echo $mostrar['salario'] ?></td>
                    <td><?php echo $mostrar['departamento'] ?></td>
                    <td><input type="submit" name="accion" value="borrar"></td>
                    <td><input type="submit" name="accion" value="actualizar"></td>
                </form>
            </td>
        </tr>
        <?php 
        }
        ?>
    </table>    
</body>
</html>
