<?php
// Crear conexión
$host = "localhost";
$user = "root";
$password = "";
$database = "empresa_a";

try {
    $conn = mysqli_connect($host, $user, $password, $database);

    // Comprobar conexion
    if($conn) {
        die("Conexión fallada ".mysqli_connect_error());
    }
    echo "Conexión exitosa";
} catch (Exception $e) {
    echo "Error con el método mysqli_connect ".$e;
}


?>