# Creación de base de datos
CREATE DATABASE empresa_a;

USE empresa_a;

# Creación de Tablas
CREATE TABLE departamento (
    id INT AUTO_INCREMENT,
    nombre VARCHAR(40) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE empleado (
    id INT AUTO_INCREMENT,
    id_departamento INT NOT NULL,
    nombre VARCHAR(100) NOT NULL,
    salario FLOAT NOT NULL DEFAULT 0.0,
    PRIMARY KEY (id),
    FOREIGN KEY (id_departamento) REFERENCES departamento(id)
);

# datos
INSERT INTO departamento (nombre) VALUES ("Recursos Humanos"), ("Administración"), ("Zona A"), ("Zona B"); 

INSERT INTO empleado (id_departamento, nombre, salario) VALUES (1, "Rene", 10000),
                                                                (1, "Diana", 50000),
                                                                (2, "Aldo", 20000),
                                                                (4, "José", 25000),
                                                                (3, "Angel", 50000),
                                                                (3, "Maria", 50000);
                                                                
# Consulta
SELECT e.id as id, e.nombre as nombre, e.salario as salario, d.nombre as departamento
FROM empleado e
JOIN departamento d
ON (e.id_departamento = d.id);



