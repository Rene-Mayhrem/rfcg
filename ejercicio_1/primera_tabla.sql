CREATE DATABASE entrevista_tecnica;

USE entrevista_tecnica;

# Creación de la tabla
CREATE TABLE primera_tabla (
    id INT AUTO_INCREMENT,
    month VARCHAR(3) NOT NULL,
    col1 VARCHAR(3),
    col2 VARCHAR(3),
    col3 VARCHAR(3),
    col4 VARCHAR(3),
    PRIMARY KEY (id)
);

# Insertar los dos registros 
INSERT INTO primera_tabla VALUES (101, "Jan", "A", "B", NULL, "B"),
                                    (102, "Feb", "C", "A", "G", "E");

# Mostrar los valores como se pide en el ejercio 1
SELECT * FROM primera_tabla;
